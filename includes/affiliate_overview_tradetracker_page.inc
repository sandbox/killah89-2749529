<?php

function filter_options_form($form, &$form_submit) {

 $form['items_per_page'] = array(
	 '#type' => 'select',
	 '#required' => FALSE,
	 '#title' => 'Items per page',
	 '#default_value' => variable_get('items_per_page'),
   '#prefix' => '<div class="col-md-6">',
   '#suffix' => '</div>',
	 '#options' => array (
		 10 => '10',
		 25 => '25',
		 50 => '50',
		 100 => '100',
	 ),
  );
  $form['assignment_status'] = array(
 	 '#type' => 'select',
 	 '#required' => FALSE,
 	 '#title' => 'Assignment status',
 	 '#default_value' => variable_get('assignment_status'),
   '#prefix' => '<div class="col-md-6">',
   '#suffix' => '</div>',
 	 '#options' => array (
 		 accepted => 'Accepted',
 		 notsignedup => 'Not signed up',
 		 pending => 'Pending',
 		 rejected => 'Rejected',
     onhold => 'On hold',
     signedout => 'Signed out',
 	 ),
   );

	$form['submit'] = array(
    '#prefix' => '<div class="col-md-12">',
    '#suffix' => '</div>',
    '#value' => 'Submit',
    '#type' => 'submit',
    );

  return $form;
}

function filter_options_form_submit(&$form_state) {
	variable_set('items_per_page', $form_state['items_per_page']['#value']);
	variable_set('assignment_status', $form_state['assignment_status']['#value']);
}

function tradetracker_GetAllPrograms($ID) {
  // stub function for collecting the items
  $records = _affiliate_overview_tradetracker_campaigns($ID);

  foreach ($records as $key => $value) {
    if ($value->info->assignmentStatus == 'accepted') {
      $a_status = 'Accepted';
    }
    if ($value->info->assignmentStatus == 'pending') {
      $a_status = 'Pending';
    }
    if ($value->info->assignmentStatus == 'notsignedup') {
      $a_status = 'Not signed up';
    }
    if ($value->info->assignmentStatus == 'rejected') {
      $a_status = 'Rejected';
    }
    if ($value->info->assignmentStatus == 'onhold') {
      $a_status = 'On hold';
    }
    if ($value->info->assignmentStatus == 'signedout') {
      $a_status = 'Signed out';
    }
  }

  $header = array(t('Name'), t('PPL &euro;'), t('PPS %'), t('PPS &euro;'), t('Assignment status'));
  $total = count($records);

  if ($total > 0) {
    // we don't have a pager query so we have initialize the pages ourselves.
    $page = isset($_GET['page']) ? $_GET['page'] : 0;
    $records_page = array_slice($records, variable_get('items_per_page') * $page, variable_get('items_per_page'));
    pager_default_initialize($total, variable_get('items_per_page'), $element = 0);
    $rows = array();
    foreach ($records_page as $program) {
      $rows[] = array('<a href="/tradetracker/program/' . $program->ID . '" class="program-name">' . $program->name . '</a>', '<div class="commission">' . number_format($program->info->commission->leadCommission, 2) . ' &euro; </div>', '<div class="commission">' . number_format($program->info->commission->saleCommissionVariable, 2) . ' % </div>', '<div class="commission">' . number_format($program->info->commission->saleCommissionFixed, 2) . ' &euro; </div>', '<div class="a-status ' . $program->info->assignmentStatus . '">' . $a_status . '</div>');
    }
  } else {
    $rows[] =array('data' => array(array('data' => t('Sorry, but no programs were found.'), 'colspan' => count($header))), 'class' => array('empty'));
  }
  if ($total == 0) {
    $output = '<h4>Sorry, but no programs were found.</h4>';
  } else {
    $output = '<h4>We have ' . $total . ' programs found</h4>';
  }
  if ($total == 1) {
    $output = '<h4>We have ' . $total . ' program found</h4>';
  }
    $output .= drupal_render(drupal_get_form('filter_options_form'));
    $output .= '<div class="col-md-12">';
    $output .= theme('table', array('header' => $header,'rows' => $rows ));
    $output .= theme('pager', array('quantity' => '5'));
    $output .= '</div>';
    return $output;
  }

function tradetracker_GetProgram($ID) {
  $programs = _affiliate_overview_tradetracker_campaigns($ID, '', '');
  dpm($programs);
    $vouchers = _affiliate_overview_tradetracker_voucher($ID, '', '');
    dpm($vouchers);

  ctools_include('modal');
  ctools_modal_add_js();

  foreach ($programs as $key => $program) {
  }
  drupal_set_title($program->name);

  $output = "<div class='col-md-8'><img src=" . $program->info->imageURL . "></div>";
  $output .= "<div class='col-md-4 right-sidebar'><h2>" . t('Commissions') . "</h2>";
  $output .= "<b>" . t('Lead') . ": </b>" . number_format($program->info->commission->leadCommission, 2) . " &euro;<br>";
  $output .= "<b>" . t('Sale %') . ": </b>" . number_format($program->info->commission->saleCommissionVariable, 2) . " %<br>";
  $output .= "<b>" . t('Sale &euro;') . ": </b>" . number_format($program->info->commission->saleCommissionFixed, 2) . " &euro;";
  $output .= "<h2 class='header-title' data-spy='affix' data-offset-top='60' data-offset-bottom='200'>" . t('Navigation') . "</h2>";
  $output .= "<ul class='nav'><li><a href='#description'>" . t('Description') . "</a></li>";
  $output .= "<li><a href='" . $program->ID . "/nojs' class='ctools-use-modal'>" . t('Form') . "</a></li>";
  $output .= "<li><a href='" . $program->info->trackingURL . "' target='_blank'>" . t('Link') . "</a></li></ul></div></div>";
  $output .= "<div class='col-md-8'><b>Categories: </b>" . $program->info->category->name . "</div>";
  $output .= "<div class='col-md-8'><div class='panel panel-default'><div class='panel-heading' id='description'>" . t('Description') . "</div>";
  $output .= "<div class='panel-body'>" . $program->info->campaignDescription . "</div></div></div>";
  $output .= "<div class='col-md-8'><table class='table table-hover' id='voucher'><thead><h2>" . t('Voucher') . "</h2>";
  $output .= "<tr>";
  $output .= "<th>" . t('Title') . "</th><th>" . t('Description') . "</th>";
  $output .= "</tr>";
  $output .= "</thead>";
  foreach ($vouchers as $key => $voucher) {
    $voucher_name = $voucher->name;
    $voucher_description = $voucher->description;
  $output .= "<tbody>";
  $output .= "<tr>";
  $output .= "<td>" . $voucher_name . "</td>";
  $output .= "<td>" . $voucher_description . "</td>";
  $output .= "</td>";
  $output .= "</thead>";
}
  $output .= "</table>";

  return $output;
}
 ?>
