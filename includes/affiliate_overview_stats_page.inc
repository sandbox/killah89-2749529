<?php

require_once('affiliate_overview.api.inc');

function affiliate_overview_stats_page()
{
  $page = array
  (
    '#prefix' => '<div id="affiliate_overview_stats_page">',
    '#suffix' => '</div>',
    'affilinet_graph' => _affiliate_overview_affilinet_graph(),
    'affilinet_pie' => _affiliate_overview_affilinet_pie(),
    'belboon_graph' => _affiliate_overview_belboon_graph(),
    'zanox_graph' => _affiliate_overview_zanox_graph(),
    'tradetracker_graph' => _affiliate_overview_tradetracker_graph(),
  );

  return $page;
}

function _affiliate_overview_zanox_graph() {
  $zanox = _affiliate_overview_zanox_stats();
}

function _affiliate_overview_tradetracker_graph() {
  $tradetracker = _affiliate_overview_tradetracker_stats();
}

function _affiliate_overview_affilinet_graph() {
  if (variable_get('affilinet_checkbox', TRUE)) {
    $affilinet = _affiliate_overview_affilinet_stats();

    if ($affilinet->CurrentMonth->Confirmed == "0" and $affilinet->PreviousMonths->Open == "0" and $affilinet->PreviousMonths->Cancelled == "0" and $affilinet->Payments->TotalPayment == "0") {
      return array('#markup' => "<div class='no-stats'><h2>" . t("Affilinet revenues") . "</h2>" . t("No statistics found for Affilinet.") . "</div>");
    }
    else {
  $chart = array(
    '#type' => 'chart',
    '#chart_type' => 'bar',
    '#title' => t('Affilinet revenues'),
    '#chart_library' => 'highcharts',
    '#stacking' => FALSE,
    );

  $color = '#8BC34A';
  $chart['confirmed'] = array(
    '#type' => 'chart_data',
    '#title' => t('Confirmed'),
    '#labels' => array(t('Confirmed')),
    '#data' => array(floatval($affilinet->CurrentMonth->Confirmed)),
    '#color' => $color,
  );
  $color = '#FFC107';
  $chart['open'] = array(
    '#type' => 'chart_data',
    '#title' => t('Open'),
    '#labels' => array(t('Open')),
    '#data' => array(floatval($affilinet->PreviousMonths->Open)),
    '#color' => $color,
  );
  $color = '#D32F2F';
  $chart['cancelled'] = array(
    '#type' => 'chart_data',
    '#title' => t('Cancelled'),
    '#labels' => array(t('Cancelled')),
    '#data' => array(floatval($affilinet->PreviousMonths->Cancelled)),
    '#color' => $color,
  );
  $color = '#2095F2';
  $chart['total_payments'] = array(
    '#type' => 'chart_data',
    '#title' => t('Total Payments'),
    '#labels' => array(t('Total Payments')),
    '#data' => array(floatval($affilinet->Payments->TotalPayment)),
    '#color' => $color,
  );

  $graph['chart'] = $chart;

  return $chart;
  }
}
}

function _affiliate_overview_affilinet_pie() {
  if (variable_get('affilinet_checkbox', TRUE)) {
    $affilinet = _affiliate_overview_affilinet_stats();

    if ($affilinet->CurrentMonth->Confirmed =="0" and $affilinet->PreviousMonths->Confirmed == "0") {
      return array('#markup' => "<div class='no-stats'><h2>" . t("Affilinet revenues") . "</h2>" . t("No statistics found for Affilinet.") . "</div>");
    }
    else {
  $pie = array(
    '#type' => 'chart',
    '#chart_type' => 'pie',
    '#title' => t('Earnings'),
    '#chart_library' => 'highcharts',
    '#legend_position' => 'right',
    '#data_labels' => FALSE,
    '#tooltips' => TRUE,
  );
  $pie['pie_data'] = array(
    '#type' => 'chart_data',
    '#title' => t('Earnings this and last month'),
    '#data' => array(array('This Month', floatval($affilinet->CurrentMonth->Confirmed)), array('Last Month', floatval($affilinet->PreviousMonths->Confirmed))),
  );

  $graph['chart'] = $pie;

  return $pie;
}
}
}

function _affiliate_overview_belboon_graph() {
  if (variable_get('belboon_checkbox', TRUE)) {
    $belboon = _affiliate_overview_belboon_stats();
    if ($belboon->handler->commissionsApproved['EUR'] == "NULL" and $belboon->handler->commissionsPending['programs']['EUR'] == "NULL") {
      return array('#markup' => "<div class='no-stats'><h2>" . t("Belboon revenues") . "</h2>" . t("No statistics found for Belboon.") . "</div>");
    }
    else {
  $chart = array(
    '#type' => 'chart',
    '#chart_type' => 'column',
    '#title' => t('Belboon revenues'),
    '#chart_library' => 'highcharts',
    '#stacking' => TRUE,
    );

  $color = '#7cb5ec';
  $chart['confirmed'] = array(
    '#type' => 'chart_data',
    '#title' => t('Confirmed'),
    '#labels' => array(t('Confirmed')),
    '#data' => array(floatval($belboon->handler->commissionsApproved['EUR'])),
    '#color' => $color,
  );
  $color = '#434348';
  $chart['open'] = array(
    '#type' => 'chart_data',
    '#title' => t('Open'),
    '#labels' => array(t('Open')),
    '#data' => array(floatval($belboon->handler->commissionsPending['programs']['EUR'])),
    '#color' => $color,
  );
  $chart['xaxis'] = array(
      '#type' => 'chart_xaxis',
      '#labels' => array('Confirmed', 'Open'),
    );

  $graph['chart'] = $chart;

  return $graph;
  }
}
}
