<?php

require_once('affiliate_overview.api.inc');

function affilinet_GetPrograms($programIds, $query, $categoryIds, $title, $currentPage = 1, &$totalResults = 0) {

	$result = _affiliate_overview_affilinet_GetPrograms($programIds,$query,$categoryIds,$currentPage);

	// affilinet_print_pre($result);
	$totalResults = $result->TotalResults;

	$output = "<h3>" . $totalResults . " " . t('Programs for ') . "'" . $title . "' " .t('found') . ":</h3>";

	if ($result->TotalResults == 0) {
		$output .= t('No programs for ') . "'" . $title . "' " . t('found');
	} else if ($result->TotalResults == 1) {
		$ppl_min = "";
		$ppl_max = "";
		$pps_min = "";
		$pps_max = "";
		if (isset($program->CommissionTypes->CommissionTypeDetail)) {
			if (is_array($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail)) {
				foreach ($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail as $commission) {
					if ($commission->CommissionTypeEnum == 'Sale') {
						$pps_min = $commission->VolumeMin . '%';
						$pps_max = $commission->VolumeMax . '%';
					}
					if ($commission->CommissionTypeEnum == 'Lead') {
						$ppl_min = $commission->VolumeMin . '&euro;';
						$ppl_max = $commission->VolumeMax . '&euro;';
					}
				}
			} else {
				if ($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Sale') {
					$pps_min = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMin;
					$pps_max = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMax;
				}
				if ($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Lead') {
					$ppl_min = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMin;
					$ppl_max = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMax;
				}
			}
		}
		$output .= "<table id='affilinet_programs' class='affilinet-table col-md-12'><tr><th rowspan='2' colspan='2'>" . t('Program') . "</th><th colspan='2'>PPL &euro;</th><th colspan='2'>PPS %</th></tr>";
		$output .= "<tr><th>min</th><th>max</th><th>min</th><th>max</th></tr>";
		$output .= "<tr><td valign='top'><a href='/affilinet/program/" . $result->ProgramCollection->Program->ProgramId . "'><img align='left' src='" . $result->ProgramCollection->Program->LogoURL . "' ><a></td><td><a href='/affilinet/program/" . $result->ProgramCollection->Program->ProgramId . "'>" . $result->ProgramCollection->Program->ProgramTitle . "</a></td><td class='affilinet_tbl_right'>" . $ppl_min . "</td><td class='affilinet_tbl_right'>" . $ppl_max. "</td><td class='affilinet_tbl_right'>" . $pps_min . "</td><td class='affilinet_tbl_right'>" . $pps_max . "</td></tr>";
		$output .= "</table>";
		$output .= "<p style='font-size:x-small;'>PPL = Pay per Lead<br>PPS = Pay per Sale</p>";

	} else {
		$output .= "<table id='affilinet_programs' class='affilinet-table col-md-12'><tr><th rowspan='2' colspan='2'><div class='header-title left'>" . t('Program') . "</div></th><th colspan='2'><div class='header-title right ppl'>PPL &euro;</div></th><th colspan='2'><div class='header-title right pps'>PPS %</div></th></tr>";
		$output .= "<tr><th><div class='header-title right bottom min'>min</div></th><th><div class='header-title right bottom max'>max</div></th><th><div class='header-title right bottom min'>min</div></th><th><div class='header-title right bottom max'>max</div></th></tr>";
		foreach($result->ProgramCollection->Program as $program) {

		$ppl_min = "";
		$ppl_max = "";
		$pps_min = "";
		$pps_max = "";
		if (isset($program->CommissionTypes->CommissionTypeDetail)) {
			if (is_array($program->CommissionTypes->CommissionTypeDetail)) {
				foreach ($program->CommissionTypes->CommissionTypeDetail as $commission) {
					if ($commission->CommissionTypeEnum == 'Sale') {
						$pps_min = $commission->VolumeMin . '%';
						$pps_max = $commission->VolumeMax . '%';
					}
					if ($commission->CommissionTypeEnum == 'Lead') {
						$ppl_min = $commission->VolumeMin . '&euro;';
						$ppl_max = $commission->VolumeMax . '&euro;';
					}
				}
			} else {
				if ($program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Sale') {
					$pps_min = $program->CommissionTypes->CommissionTypeDetail->VolumeMin . '%';
					$pps_max = $program->CommissionTypes->CommissionTypeDetail->VolumeMax . '%';
				}
				if ($program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Lead') {
					$ppl_min = $program->CommissionTypes->CommissionTypeDetail->VolumeMin . '&euro;';
					$ppl_max = $program->CommissionTypes->CommissionTypeDetail->VolumeMax . '&euro;';
				}
			}
		}
			$output .= "<tr><td valign='top'><a href='/affilinet/program/" . $program->ProgramId . "'><img align='left' src='" . $program->LogoURL . "' ></a></td><td><a href='/affilinet/program/" . $program->ProgramId . "'>" . $program->ProgramTitle . "</a></td><td class='affilinet_tbl_right'>" . $ppl_min . "</td><td class='affilinet_tbl_right'>" . $ppl_max . "</td><td class='affilinet_tbl_right'>" . $pps_min . "</td><td class='affilinet_tbl_right'>". $pps_max . "</td></tr>";
		}
		$output .= "</table>";
		$output .= "<p style='font-size:x-small;'>" . t("PPL = Pay per Lead") . "<br>" . t("PPS = Pay per Sale") . "</p>";
	}

	return $output;
}

function affilinet_GetCashbacks($programIds, $query, $categoryIds, $title, $currentPage = 1, &$totalResults = 0) {

    $result = _affiliate_overview_affilinet_GetPrograms($programIds,$query,$categoryIds,$currentPage);

    // affilinet_print_pre($result);
    $totalResults = $result->TotalResults;

    $output = "<h3>" . $totalResults . " " . t('Cashback programs ') . " " . t('found') . ":</h3>";

    if ($result->TotalResults == 0) {
        $output .= t('No programs for ') . "'" . $title . "' " . t('found');
    } else if ($result->TotalResults == 1) {
        $ppl_min = "";
        $ppl_max = "";
        $pps_min = "";
        $pps_max = "";
        if (isset($program->CommissionTypes->CommissionTypeDetail)) {
            if (is_array($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail)) {
                foreach ($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail as $commission) {
                    if ($commission->CommissionTypeEnum == 'Sale') {
                        $pps_min = $commission->VolumeMin . '%';
                        $pps_max = $commission->VolumeMax . '%';
                    }
                    if ($commission->CommissionTypeEnum == 'Lead') {
                        $ppl_min = $commission->VolumeMin . '&euro;';
                        $ppl_max = $commission->VolumeMax . '&euro;';
                    }
                }
            } else {
                if ($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Sale') {
                    $pps_min = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMin;
                    $pps_max = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMax;
                }
                if ($result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Lead') {
                    $ppl_min = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMin;
                    $ppl_max = $result->ProgramCollection->Program->CommissionTypes->CommissionTypeDetail->VolumeMax;
                }
            }
        }
        $output .= "<table id='affilinet_programs' class='affilinet-table col-md-12'><tr><th rowspan='2' colspan='2'>" . t('Program') . "</th><th colspan='2'>PPL &euro;</th><th colspan='2'>PPS %</th></tr>";
        $output .= "<tr><th>min</th><th>max</th><th>min</th><th>max</th></tr>";
        $output .= "<tr><td valign='top'><img align='left' src='" . $result->ProgramCollection->Program->LogoURL . "' ></td><td>" . $result->ProgramCollection->Program->ProgramTitle . "</td><td class='affilinet_tbl_right'>" . $ppl_min . "</td><td class='affilinet_tbl_right'>" . $ppl_max. "</td><td class='affilinet_tbl_right'>" . $pps_min . "</td><td class='affilinet_tbl_right'>". $pps_max . "</td></tr>";
        $output .= "</table>";
        $output .= "<p style='font-size:x-small;'>PPL = Pay per Lead<br>PPS = Pay per Sale</p>";

    } else {
        $output .= "<table id='affilinet_programs' class='affilinet-table col-md-12'><tr><th rowspan='2' colspan='2'>" . t('Program') . "</th><th colspan='2'>PPL &euro;</th><th colspan='2'>PPS %</th></tr>";
        $output .= "<tr><th>min</th><th>max</th><th>min</th><th>max</th></tr>";
        foreach($result->ProgramCollection->Program as $program) {

        $ppl_min = "";
        $ppl_max = "";
        $pps_min = "";
        $pps_max = "";
        if (isset($program->CommissionTypes->CommissionTypeDetail)) {
            if (is_array($program->CommissionTypes->CommissionTypeDetail)) {
                foreach ($program->CommissionTypes->CommissionTypeDetail as $commission) {
                    if ($commission->CommissionTypeEnum == 'Sale') {
                        $pps_min = $commission->VolumeMin . '%';
                        $pps_max = $commission->VolumeMax . '%';
                    }
                    if ($commission->CommissionTypeEnum == 'Lead') {
                        $ppl_min = $commission->VolumeMin . '&euro;';
                        $ppl_max = $commission->VolumeMax . '&euro;';
                    }
                }
            } else {
                if ($program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Sale') {
                    $pps_min = $program->CommissionTypes->CommissionTypeDetail->VolumeMin . '%';
                    $pps_max = $program->CommissionTypes->CommissionTypeDetail->VolumeMax . '%';
                }
                if ($program->CommissionTypes->CommissionTypeDetail->CommissionTypeEnum == 'Lead') {
                    $ppl_min = $program->CommissionTypes->CommissionTypeDetail->VolumeMin . '&euro;';
                    $ppl_max = $program->CommissionTypes->CommissionTypeDetail->VolumeMax . '&euro;';
                }
            }
        }
            $output .= "<tr><td valign='top'><img align='left' src='" . $program->LogoURL . "' ></td><td>" . $program->ProgramTitle . "</td><td class='affilinet_tbl_right'>" . $ppl_min . "</td><td class='affilinet_tbl_right'>" . $ppl_max . "</td><td class='affilinet_tbl_right'>" . $pps_min . "</td><td class='affilinet_tbl_right'>". $pps_max . "</td></tr>";
        }
        $output .= "</table>";
        $output .= "<p style='font-size:x-small;'>" . t("PPL = Pay per Lead") . "<br>" . t("PPS = Pay per Sale") . "</p>";
    }

    return $output;
}

function affilinet_GetShopList($query) {

	$result = _affiliate_overview_affilinet_GetShopList($query);
	// $result = _affiliate_overview_affilinet_GetCategoryList(163);

	affilinet_print_pre($result);

	return "";
}


function affilinet_GetProgramListByCategory($CategoryID,$CategoryName = "") {
	return affilinet_GetPrograms('','',array($CategoryID),$CategoryName);
}

function items_per_page_form($form, &$form_submit) {

 $form['items_per_page'] = array(
	 '#type' => 'select',
	 '#required' => FALSE,
	 '#title' => 'Items per page',
	 '#default_value' => 10,
	 '#options' => array (
		 10 => '10',
		 25 => '25',
		 50 => '50',
		 100 => '100',
	 ),
  );

	$form['submit'] = array(
    '#value' => 'Submit',
    '#type' => 'submit',
    );

  return $form;
}

function items_per_page_form_submit(&$form_state) {
	variable_set('items_per_page', $form_state['items_per_page']['#value']);
}

function affilinet_GetAllPrograms($page) {

    if ($page == "") {
        $page = 1;
    }

		$output = '';
		$output .= drupal_render(drupal_get_form('items_per_page_form'));

    $output .= affilinet_GetPrograms('','','','Alle',$page,$totalResults);
    $pages = 1 + ($totalResults - ($totalResults % 25)) / 25;

    $nextpage = $page + 1;
    $prevpage = $page - 1;

    if ($pages == 1) { // nur eine Seite
        return $output;
    }

    if ($page == 1 && $page < $pages) {
        $output .= "<div class='pager'><a href='/partnerprograms/all/2'>Weiter >></a></div>";
    } else {
        if ($page == $pages) {
        $output .= "<div class='pager'><a href='/partnerprograms/all/" . $prevpage ."'><< Zurück</a></div>";
    } else {
        $output .= "<div class='pager'><a href='/partnerprograms/all/" . $prevpage ."'><< Zurück</a> | ";
        $output .= "<a href='/partnerprograms/all/" . $nextpage ."'>Weiter >></a></div>";
    }
    }

    return $output;
}

function affilinet_GetProgram($ProgramId) {

	$result = _affiliate_overview_affilinet_GetPrograms(array($ProgramId), '', '');
	dpm($result);

	drupal_set_title($result->ProgramCollection->Program->ProgramTitle);

	$output = "<div class='Logo'><img src='" . $result->ProgramCollection->Program->LogoURL . "' /></div>";
	$output .= "<div class='ProgramDescription'>" . $result->ProgramCollection->Program->ProgramDescription . "</a></div>";

	// Provisionen
	$rates = _affiliate_overview_affilinet_GetProgramRates($ProgramId);
	$output .= "<br><form><fieldset><legend><b>Provisionen</b></legend>";
	$output .= "<div class='green'>";
	if (count($rates->RateCollection) > 1) {
		foreach ($rates->RateCollection as $rate) {
			$output .= $rate->RateMode . ", " . $rate->RateName . ", " . $rate->RateValue . $rate->Unit . "<br>";
		}
	} else {
		$output .= $rates->RateCollection->RateMode . ", " . $rates->RateCollection->RateName . ", " . $rates->RateCollection->RateValue . $rates->RateCollection->Unit . "<br>";
	}
	$output .= "</div>";
	$output .= "</fieldset></form>";

	// Vouchers
	$codes = affilinet_SearchVoucherCodes(1, $ProgramId);
	$output .= "<form><fieldset><legend><b>Gutscheine & Aktionen</b></legend>";
	$output .= "<div class='yellow'>";
	$output .= $codes;
	$output .= "</div>";
	$output .= "</fieldset></form>";

	// Limitations
	$output .= "<form><fieldset><legend><b>Richtlinien</b></legend>";
	if (($result->ProgramCollection->Program->LimitationsComment <> '')) {
		$output .= "<div class='LimitationsComment pink'>" . $result->ProgramCollection->Program->LimitationsComment . "</a></div>";
	} else {$output .= t('No Limitations.'); }
	$output .= "</fieldset></form>";

	// Statistiken
	$statistics = _affiliate_overview_affilinet_GetProgramStatistics(array($ProgramId));  // das interessanteste
	//affilinet_print_pre($statistics);
	$output .= "<form><fieldset><legend><b>Statistiken</b></legend>";
	if (isset($statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord)) {
		$output .= t('Clickthrough: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->Clickthrough . "<br>";
		$output .= t('Views: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->Views . "<br>";
		$output .= t('Clicks: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->Clicks . "<br>";
		$output .= t('Sales: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->Sales . "<br>";
		$output .= t('Open Sales: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->OpenSales . "<br>";
		$output .= t('Cancelled Sales: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->CancelledSales . "<br>";
		$output .= t('Commission: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->Commission . "&euro;" . "<br>";
		$output .= t('Open Commission: ') . $statistics->ProgramStatisticsRecords->PayPerSaleLeadStatistics->StatisticsRecords->ProgramStatisticsRecord->OpenCommission . "&euro;";
	} else {
		$output .= t('No statistics available for this program.');
	}
	$output .= "</fieldset></form>";

	// Infos
	$output .= "<form><fieldset><legend><b>Programm Infos</b></legend>";
	$output .= "Program ID: " . $ProgramId . "<br>";
	$output .= "Programstatus: " . $result->ProgramCollection->Program->PartnershipStatus;
	$output .= "</fieldset></form>";

	// Creatives
	$creatives = _affiliate_overview_affilinet_SearchCreatives(array($ProgramId));
	//$creativeCategories = _affiliate_overview_affilinet_GetCreativeCategories($ProgramId);

	//affilinet_print_pre($creativeCategories);
	//affilinet_print_pre($creatives);

	$output .= "<h3>" . t('Banners & Links') . "</h3>";


	foreach ($creatives->CreativeCollection->Creative as $banner) {
		if ($banner->CreativeTypeEnum == "Banner") {
			$code = "<a target='_blank' href='https://partners.webmasterplan.com/click.asp?ref=" . AFFILINET_USERNAME . "&site=" . $ProgramId . "&type=b1&bnb=1&subid=" . $user->uid . "'>";
			$code .= "<img src='" . $banner->BannerStub->BannerURL . "' /></a>";
			$code = $banner->IntegrationCode;

			// Create link with subId
			$script_parts = explode('&target=', $code);
			$script = $script_parts[0] . '&subid=' . $user->uid;
			if (isset($script_parts[1])) {$script .= '&target=' . $script_parts[1];}

			$link_parts = explode('" target', $script);
			$link = $link_parts[0] . '&subid=' . $user->uid;
			if (isset($link_parts[1])) {$link .= '" target' . $link_parts[1];}

			$image_parts = explode('" border', $link);
			$image = $image_parts[0] . '&subid=' . $user->uid;
			if (isset($image_parts[1])) {$image .= '" border' . $image_parts[1];}

			$code = $image;

			$href1=substr($code,strpos($code, "href")+6);
			$href=substr($href1,0,strpos($href1, " target")-1);

			$output .= "<img src='" . $banner->BannerStub->BannerURL . "' /></a>";
			$output .= "<div><br><b>" . $banner->Title . "</b><br>" . $code . "</div>";
			$output .= "<div>Link:<br><textarea rows='1' cols='100'>" . $href . "</textarea></div>";
			$output .= "<div>Banner:<br><textarea rows='5' cols='100'>" . $code . "</textarea></div><hr>";
		}
	}

	// Statistics
	//$statistics = _affiliate_overview_affilinet_GetDailyStatistics($ProgramId);
	//$statistics = _affiliate_overview_affilinet_GetPublisherStatisticsPerClick($ProgramId);
	//$statistics = _affiliate_overview_affilinet_GetPublisherClicksSummary($ProgramId,$user->uid);
	//$statistics = _affiliate_overview_affilinet_GetClicksBySubIdPerDay($ProgramId);

	//affilinet_print_pre($statistics);

	return $output;
}

function affilinet_subId_Statistics($uid) {
global $user;

	if($uid=='') {
		$uid = $user->uid;
	}

    if ($uid=='alle') {
        $uid = '';
    }

    if ($uid=='demo'){
        $uid='1';
        drupal_set_title("Cashback Provisionen Demo");
    }

	//$payments = _affiliate_overview_affilinet_GetPayments();
	//dpm($payments);

	//$transactions = _affiliate_overview_affilinet_GetTransactions(11757,$uid);
	//dpm($transactions);

	//$DailyStatistics = _affiliate_overview_affilinet_GetDailyStatistics(0,$uid,14);
	$SalesLeadsStatistics = _affiliate_overview_affilinet_GetSalesLeadsStatistics(array(),$uid);
	$SubIdStatistics = _affiliate_overview_affilinet_GetSubIdStatistics($uid);
	//dpm($SubIdStatistics);

	if ($SalesLeadsStatistics->SalesLeadsStatisticsRecords->TotalConfirmed == '0' && $SalesLeadsStatistics->SalesLeadsStatisticsRecords->TotalOpen == '0' && $SalesLeadsStatistics->SalesLeadsStatisticsRecords->TotalCancelled == '0') {
			$output = t('No commissions available!') . "<br>" . t('Wrong country selected?');
			$output .= "<p>&nbsp;</p><p><a class='button' href='/affilinet/commissions/demo'>" . t('Show demo commissions') . "</a>";
            return $output;
	}

	$header = array(
	    array('data' => t('Member'),'field' => 'user'),
		array('data' => t('Date'),'field' => 'date'),
		array('data' => t('Program'),'field' => 'program'),
		array('data' => t('Price'),'field' => 'price'),
		array('data' => t('Commission'),'field' => 'commission'),
		array('data' => t('Confirmed'),'field' => 'confirmed'),
        array('data' => t('Am'),'field' => 'checkdate'),
		array('data' => t('Status'),'field'=>'status'),
	);

	$rows = array();
    if (is_array($SubIdStatistics->SubIdStatisticsRecords->Records->SubIdStatisticsRecord)) {
    	foreach ($SubIdStatistics->SubIdStatisticsRecords->Records->SubIdStatisticsRecord as $val) {
    	    //dpm($val);
    	    if (isset($val->CheckDate) && ($val->CheckDate != '0001-01-01T00:00:00')) {
    	       $checkdate = format_date(strtotime($val->CheckDate), 'custom', 'j. F Y');
            } else {
                $checkdate = t("Not confirmed");
            }
            $rows[]=array('data'=>array(
              'user' => user_load($val->SubId)->name,
              'date'=> format_date(strtotime($val->Date), 'custom', 'j. F Y'),
              'program'=>'<a href="/affilinet/program/' . $val->ProgramId . '">' . $val->ProgramTitle . '</a>',
              'price'=> array('data' => $val->Price . '&euro;', 'class' => 'affilinet_tbl_right'),
              'commission'=> array('data' => $val->Commission . '&euro;', 'class' => 'affilinet_tbl_right'),
              'confirmed'=> array('data' => $val->Confirmed . '&euro;', 'class' => 'affilinet_tbl_right'),
              'checkdate'=> $checkdate,
              'status' => array('data' => t($val->TransactionStatus), 'class' => 'affilinet_tbl_right affilinet_status_' . $val->TransactionStatus),
            ));
    	}
    } else { // nur 1 Ergebnis:
        foreach ($SubIdStatistics->SubIdStatisticsRecords->Records as $val) {
        //dpm($val);
        if (isset($val->CheckDate) && ($val->CheckDate != '0001-01-01T00:00:00')) {
           $checkdate = format_date(strtotime($val->CheckDate), 'custom', 'j. F Y');
        } else {
            $checkdate = t("Not checked");
        }
        $rows[]=array('data'=>array(
          'user' => user_load($val->SubId)->name,
          'date'=> format_date(strtotime($val->Date), 'custom', 'j. F Y'),
          'checkdate'=> $checkdate,
          'program'=>$val->ProgramTitle,
          'price'=> array('data' => $val->Price . '&euro;', 'class' => 'affilinet_tbl_right'),
          'commission'=> array('data' => $val->Commission . '&euro;', 'class' => 'affilinet_tbl_right'),
          'confirmed'=> array('data' => $val->Confirmed . '&euro;', 'class' => 'affilinet_tbl_right'),
          'status' => array('data' => $val->TransactionStatus, 'class' => 'affilinet_tbl_right affilinet_status_' . $val->TransactionStatus),
        ));
    }
    }
	$output = "<h3>" . t('Commissions') . "</h3>";
	$output .= "<table class='affilinet-table'>";
	// $output .= "<tr><td width='200px'>" . t('Total bought: ') . "</td><td  class='affilinet_tbl_right' width='30px'>" . $SubIdStatistics->SubIdStatisticsRecords->TotalPrice . "&euro;</td></tr>";
	$output .= "<tr><td width='200px'>" . t('Commision confirmed: ') . "</td><td  class='affilinet_tbl_right' width='30px'>" . $SalesLeadsStatistics->SalesLeadsStatisticsRecords->TotalConfirmed . "&euro;</td></tr>";
	$output .= "<tr><td>" . t('Commision open: ')  . "</td><td class='affilinet_tbl_right'>" . $SalesLeadsStatistics->SalesLeadsStatisticsRecords->TotalOpen . "&euro;</td></tr>";
	$output .= "<tr><td>" . t('Commision cancelled: ')  . "</td><td class='affilinet_tbl_right'>" . $SalesLeadsStatistics->SalesLeadsStatisticsRecords->TotalCancelled . "&euro;</td></tr>";
	$output .= "</table>";
	$output .= "<p>&nbsp;</p>";

	$output .= theme('table', array(
		'header' => $header,
		'rows' => $rows,
	));

	return $output;
}

function affilinet_GetPayments() {
	$result = _affiliate_overview_affilinet_GetPayments();
	affilinet_print_pre($result);

	return "";
}


function affilinet_SearchVoucherCodes_form($form, &$form_state) {

	$form['search'] = array(
		'#type' => 'textfield',
		'#title' => t('Searchphrase'),
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('search'),
	);

    // $form_state['storage'] is only set when the form
    // as been submitted (see the _submit() function for
    // more details)
    if(isset($form_state['storage'], $form_state['storage']['search']))
    {
        $form['result_display'] = array(
            '#markup' => affilinet_SearchVoucherCodes(1,'',$form_state['storage']['search']),
            '#prefix' => '<div>',
            '#suffix' => '</div>',
        );
    }
    return $form;

}

function affilinet_SearchVoucherCodes_form_submit($form, &$form_state) {
    $form_state['storage']['search'] = $form_state['values']['search'];
    $form_state['rebuild'] = TRUE;
}


function affilinet_SearchVoucherCodes($currentPage = 1, $programId = '', $query = ''){
global $user;

	if ($currentPage == 0) {$currentPage = 1;}
	$codes= _affiliate_overview_affilinet_SearchVoucherCodes($currentPage, $programId, $query);
	// affilinet_print_pre($codes, "Codes: ");

	if (!isset($codes->VoucherCodeCollection->VoucherCodeItem)) {
		return t('No vouchers available.');
	}

	$output = "<table class='affilinet-table'>";
	$output .= "<tr><th>" . t('Program') . "</th><th>" . t('Title') . "</th><th>" . t('Date') . "</th><th>" . t('Code') . "</th><th>" . t('Link') . "</th></tr>";

	if (is_array($codes->VoucherCodeCollection->VoucherCodeItem)) {
		$programs = array();
		foreach ($codes->VoucherCodeCollection->VoucherCodeItem as $val) {
			if (!isset($programs[$val->ProgramId])) {
				$program = _affiliate_overview_affilinet_GetPrograms(array($val->ProgramId), '', '');
				$programs[$val->ProgramId] = $program->ProgramCollection->Program->LogoURL;
			}

			$pos = strpos($val->IntegrationCode, '">');
			$link = substr($val->IntegrationCode, 0, $pos) . '&subid=' . $user->uid . '">' . substr($val->IntegrationCode,$pos+4);
			$link = '<a target="_blank" ' . substr($link, 3);
			$link = str_replace('<br />', '', $link);

			$output .= "<tr><td><a href='/affilinet/program/" . $val->ProgramId . "'><img src='" . $programs[$val->ProgramId] . "' ></a></td>";
			$output .= "<td><b>" . $val->Title . "</b></td><td>" . format_date(strtotime($val->StartDate), 'custom', 'j.m.Y') . " -<br>" . format_date(strtotime($val->EndDate), 'custom', 'j.m.Y') . "</td><td><b>" . $val->Code . "</b></td>";
			$output .= "<td>" . $link . "</td></tr>";
			$output .= "<tr><td colspan='4'>" . $val->Description . "</td><td><textarea>" . $link . "</textarea></td></tr>";
			$output .= "<tr><td colspan='5'></td></tr>";
		}
	} else {
		$val = $codes->VoucherCodeCollection->VoucherCodeItem;
		if (!isset($programs[$val->ProgramId])) {
			$program = _affiliate_overview_affilinet_GetPrograms(array($val->ProgramId), '', '');
			$programs[$val->ProgramId] = $program->ProgramCollection->Program->LogoURL;
		}

		$pos = strpos($val->IntegrationCode, '">');
		$link = substr($val->IntegrationCode, 0, $pos) . '&subid=' . $user->uid . '">' . substr($val->IntegrationCode,$pos+4);
		$link = '<a target="_blank" ' . substr($link, 3);
		$link = str_replace('<br />', '', $link);

		$output .= "<tr><td><a href='/affilinet/program/" . $val->ProgramId . "'><img src='" . $programs[$val->ProgramId] . "' ></a></td>";
		$output .= "<td><b>" . $val->Title . "</b></td><td>" . format_date(strtotime($val->StartDate), 'custom', 'j.m.Y') . " -<br>" . format_date(strtotime($val->EndDate), 'custom', 'j.m.Y') . "</td><td><b>" . $val->Code . "</b></td>";
		$output .= "<td>" . $link . "</td></tr>";
		$output .= "<tr><td colspan='4'>" . $val->Description . "</td><td><textarea>" . $link . "</textarea></td></tr>";
		$output .= "<tr><td colspan='5'></td></tr>";
	}
	$output .= "</table>";

	$prev = intval($currentPage) - 1;
	$next = intval($currentPage) + 1;
	$total = intval($codes->TotalResults / 25) + 1;

	if ($programId == '') { // pager nicht auf Programmseite anzeigen
		$output .= "<div>";
		if ($currentPage > 1) {
			$output .= "<a class='form-submit art-button' href='/affilinet/vouchers/1'>&lt;&lt</a>&nbsp;";
			$output .= "<a class='form-submit art-button' href='/affilinet/vouchers/$prev'><</a>&nbsp;";
			}
		$output .= $currentPage . "/" . $total . "&nbsp;";
		if ($currentPage < $total) {
			$output .= "<a class='form-submit art-button' href='/affilinet/vouchers/$next'>></a>&nbsp;";
			$output .= "<a class='form-submit art-button' href='/affilinet/vouchers/$total'>&gt;&gt;</a>";
		}
		$output .= "</div>";
	}
	return $output;
}

function affilinet_GetCategoryList($shopId){
	$list = _affiliate_overview_affilinet_GetCategoryList($shopId);
	// affilinet_print_pre($list);

	$categories = $list->CategoryResult->Categories->Category;
	$parents = array();

	foreach ($categories as $a){
		$parents[$a->ParentCategoryId][] = $a;
	}

//	affilinet_print_pre($parents, "Parents");

	$output = "<table> class='affilinet-table'";
	foreach ($parents[0] as $parent) {
		$output .= "<tr><td>" . $parent->Title . "</td><td>";
		if (isset($parents[$parent->CategoryId])) {
			foreach($parents[$parent->CategoryId] as $child) {
				$output .= $child->Title . "<br>";
			}
		}
		$output .= "</td></tr>";
	}
	$output .= "</table>";


	return $output;
}
?>
