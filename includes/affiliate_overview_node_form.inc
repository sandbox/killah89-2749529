<?php
function create_node(&$form, &$form_state) {
  global $user;

  // Set Variables.
  variable_set('node_title', $form_state['values']['node_title']);
  variable_set('node_type', $form_state['values']['node_type']);
  variable_set('node_body', $form_state['values']['node_body']);
  variable_set('node_affiliate_link', $form_state['values']['node_affiliate_link']);
  variable_set('node_ablaufdatum', $form_state['values']['node_ablaufdatum']);
  variable_set('node_logo', $form_state['values']['node_logo']);

  // Get Variables.
  $node_title = variable_get('node_title');
  $node_type = variable_get('node_type');
  $node_body = variable_get('node_body');
  $node_affiliate_link = variable_get('node_affiliate_link');
  $node_ablaufdatum = variable_get('node_ablaufdatum');
  $node_logo = variable_get('node_logo');

  // Create an Entity.
  $node = entity_create('node', array('type' => $node_type));
  // Specify the author.
  $node->uid = $user->uid;
  // Create a Entity Wrapper of that new Entity.
  $emw_node = entity_metadata_wrapper('node', $node);
  // Set a title and some text field value.
  $emw_node->title = $node_title;
  $emw_node->body->set(array('value' => $node_body, 'format' => 'full_html'));
  $emw_node->body->summary->set($node_body);
  // Set affiliate link.
  $emw_node->field_affiliate_link = $node_affiliate_link;
  // Retrieve image from URL and set.
  $url = $node_logo;
  $path = 'public://' . $node_type . '/' . $node_title . '.png';
  $file_info = system_retrieve_file($url, $path, TRUE);
  if($file_info->fid){
    $node->field_logo[LANGUAGE_NONE]['0']['fid'] = $file_info->fid; //assign fid
  }
  // Set expire date.
  $node->field_ablaufdatum[LANGUAGE_NONE][0] = array(
   'value' => $node_ablaufdatum,
 );
  // And save it.
  $emw_node->save();

  drupal_set_message($node_title . t(' has been created in ' . $node_type . '.'));
}
/**
 * Ajax menu callback.
 */
function tradetracker_node_form_callback($ID, $ajax) {
  if ($ajax) {
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('MyModule Modal Form'),
    );

    // Use ctools to generate ajax instructions for the browser to create
    // a form in a modal popup.
    $output = ctools_modal_form_wrapper('create_node_form', $form_state);

    // If the form has been submitted, there may be additional instructions
    // such as dismissing the modal popup.
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }

    // Return the ajax instructions to the browser via ajax_render().
    print ajax_render($output);
    drupal_exit();
  }
  else {
    return drupal_get_form('create_node_form');
  }
}
function create_node_form($form, &$form_state) {
  $programs = _affiliate_overview_tradetracker_campaigns(arg(2));

  foreach ($programs as $key => $program) {
  }

  ctools_include('modal');
  ctools_modal_add_js();

  $TITLE = $program->name;
  $BODY = $program->info->campaignDescription;
  $LINK = $program->info->trackingURL;
  $LOGO_URL = $program->info->imageURL;

  $form['node_type'] = array(
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => array(
      'gewinnspiele' => t('Gewinnspiele'),
      'gutschein' => t('Gutscheine'),
      'shop' => t('Shop'),
    ),
  );

  $form['node_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $TITLE,
  );
  $form['node_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $BODY,
  );
  $form['node_affiliate_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Link'),
    '#default_value' => $LINK,
  );
  $form['node_ablaufdatum'] = array(
    '#type' => 'date_popup',
    '#date_timezone' => 'Europe/Berlin',
    '#date_format' => 'Y-m-d H:i:s',
    '#date_increment' => 1,
    '#date_year_range' => '-3:+3',
  );
  $form['node_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Logo URL'),
    '#default_value' => $LOGO_URL,
  );
  $form['submit'] = array(
   '#type' => 'submit',
   '#value' => t('Click Here!'),
   '#submit' => array('create_node'),
  );

 return $form;
}
