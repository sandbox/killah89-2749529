Please install soap client before performing below steps.
1. Installation
2. Configuration
3. Overview

1. Install module as usual --> https://www.drupal.org/documentation/install/modules-themes/modules-7

2. To configure affiliate networks, you must enter your credentials on admin/config/services/affiliate-overview/login for at least one network. 

3. After entering your credentials you find the overview of your balance on admin/config/services/affiliate-overview. 
