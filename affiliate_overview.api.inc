<?php

require_once 'client/ApiClient.php';

/**
 * API Calls for Affilinet webservice
 * Username and password cookie
 */
if (isset($_COOKIE['Drupal_visitor_affiliate_overview_affilinet_username_active'])) {
  define("AFFILINET_USERNAME", $_COOKIE['Drupal_visitor_affiliate_overview_affilinet_username_active']);
}
else {
  define("AFFILINET_USERNAME", variable_get('affilinet_username')); // Publisher-ID for Affilinet
}
define("AFFILINET_PASSWORD", variable_get('affilinet_password')); // Publisher webservices password for Affilinet

/**
 * API Calls for Belboon webservices
 * Username and password cookie
 */
if (isset($_COOKIE['Drupal_visitor_affiliate_overview_belboon_username_active'])) {
  define("BELBOON_USERNAME", $_COOKIE ['Drupal_visitor_affiliate_overview_belboon_username_active']);
}
else {
  define("BELBOON_USERNAME", variable_get('belboon_username')); // Publisher username for Belboon
}
define("BELBOON_PASSWORD", variable_get('belboon_password')); // Publisher webservices password for Belboon

/**
 * API Calls for Zanox webservices
 * Connectid and Secretkey cookie
 */
if (isset($_COOKIE['Drupal_visitor_affiliate_overview_zanox_connectid_active'])) {
  define("ZANOX_CONNECTID", $_COOKIE ['Drupal_visitor_affiliate_overview_zanox_connectid_active']);
}
else {
  define("ZANOX_CONNECTID", variable_get('zanox_connectid')); // Connectid for Zanox webservices
}
define("ZANOX_SECRETKEY", variable_get('zanox_secretkey')); // Secretkey for Zanox webservices

/**
 * API
 */
define("WSDL_LOGON", "https://api.affili.net/V2.0/Logon.svc?wsdl"); // Affilinet webservices authentication token
define("WSDL_ACCOUNT", "https://api.affili.net/V2.0/AccountService.svc?wsdl"); // Affilinet webservices statistics
define("WSDL_SERVER", "http://api.belboon.com/?wsdl"); // Belboon webservices statistics
define("WSDL_ZANOX", "https://api.zanox.com/wsdl/2011-03-01"); // Zanox webservices

function _affiliate_overview_affilinet_stats() {
  if (variable_get('affilinet_checkbox', TRUE)) {
    /*
     * Send request to Affilinet logon service for authentication token
     */
    try {
      $soapLogon = new SoapClient(WSDL_LOGON);
      $token = $soapLogon->Logon(array(
        'Username' => AFFILINET_USERNAME,
        'Password' => AFFILINET_PASSWORD,
        'WebServiceType' => 'Publisher'
      ));

      /**
       * Send request to Affilinet publisher statistics webservices
       */
      $soapRequest = new SoapClient(WSDL_ACCOUNT);
      $response = $soapRequest->GetPublisherSummary($token);
      return $response;
    }
    catch (Exception $e) {
      echo 'Error: ', $e->getMessage(), "\n";
      drupal_exit();
    }

  }
}

function _affiliate_overview_belboon_stats() {
  if (variable_get('belboon_checkbox', TRUE)) {
    $config = array(
      'login' => BELBOON_USERNAME,
      'password' => BELBOON_PASSWORD,
      'trace' => true
    );

    $client = new SoapClient(WSDL_SERVER, $config);
    $result = $client->getAccountInfo();
    return $result;

  }
}

function _affiliate_overview_zanox_stats() {
  if (variable_get('zanox_checkbox', TRUE)) {

    $api = ApiClient::factory(PROTOCOL_SOAP, VERSION_DEFAULT);

    $connectId = variable_get('zanox_connectid');
    $secretKey = variable_get('zanox_secretkey');

    $api->setConnectId($connectId);
    $api->setSecretKey($secretKey);

    $fromDate = strtotime('first day of ' . date('F Y'));
    $toDate = strtotime("today");
    ;

    $soap = $api->getReportBasic($fromDate, $toDate, $dateType = NULL, $currency = NULL, $programId = NULL, $admediumId = NULL, $admediumFormat = NULL, $adspaceId = NULL, $reviewState = NULL, $groupBy = NULL);

    print "<pre>";
    print_r($soap);
  }
}
